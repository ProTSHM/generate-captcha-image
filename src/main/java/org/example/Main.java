package org.example;

import org.imgscalr.Scalr;

import javax.imageio.ImageIO;
import java.awt.*;
import java.awt.geom.AffineTransform;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Random;

public class Main {
    public static void main(String[] argv) throws Exception {
        String text;
        int result = 0;
        for (int i = 0; i < 6; i++) {

            int a = (new Random().nextInt(9)) + 1;
            int b = (new Random().nextInt(9)) + 1;

            if (a - b >= 0) {
                text = " " + a + "-" + b + "=";
                result = a - b;
            } else {
                text = " " + a + "+" + b + "=";
                result = a + b;
            }

            if (a<5 && b<5){
                text = " " + a + "x" + b + "=";
                result = a * b;
            }


            genCaptcha(text);
        }


    }

    private static Random random = new Random();
    private static int width = 80 * 10;
    private static int height = 26 * 10;
    private static int lines = 100;

    public static BufferedImage genCaptcha(String code) throws IOException {
        BufferedImage image = new BufferedImage(width, height,
                BufferedImage.TYPE_INT_BGR);
        Graphics g = image.getGraphics();
        g.fillRect(0, 0, width, height);
        g.setFont(new Font("Times New Roman", Font.HANGING_BASELINE, 18));
        g.setColor(getRandColor(110, 133));
        //
        drowCode(g, code);
        //
        for (int i = 0; i <= lines; i++) {
            drowLine(g);
        }
        g.dispose();

        BufferedImage resizedImage = Scalr.resize(image, Scalr.Method.QUALITY, Scalr.Mode.FIT_TO_WIDTH,
                150, 48, Scalr.OP_ANTIALIAS);

        ImageIO.write(resizedImage, "png", new File(code + ".png"));
        return image;
    }

    private static Color getRandColor(int fc, int bc) {
        if (fc > 255)
            fc = 255;
        if (bc > 255)
            bc = 255;
        int r = fc + random.nextInt(bc - fc - 16);
        int g = fc + random.nextInt(bc - fc - 14);
        int b = fc + random.nextInt(bc - fc - 18);
        return new Color(r, g, b);
    }

    private static String drowCode(Graphics g, String code) {

        int margin = width / code.length();
        for (int i = 0; i < code.length(); ++i) {
            g.setFont(getFont(false));
            g.setColor(getRandColor(0, 100));
            g.translate(random.nextInt(7), random.nextInt(7));
            String sign = String.valueOf(code.charAt(i));
            int x = margin * i;
            int y = new Random().nextInt(100) + 135;
            int r = -30 + new Random().nextInt(60);
            if (sign.equals("+") || sign.equals("-") || sign.equals("=")|| sign.equals("x")) {
                y = 190;
                r = 0;
                g.setFont(getFont(true));
            }
//            g.drawString(sign, x, y);


            Graphics2D g2d = (Graphics2D) g;


            AffineTransform old = g2d.getTransform();

            g2d.rotate(Math.toRadians(r), x, y);
            g2d.drawString(sign, x, y);
            g2d.setTransform(old);


        }
        return code;
    }

    private static void drowLine(Graphics g) {
        Graphics2D g2 = (Graphics2D) g;
        g2.setStroke(new BasicStroke(2));
        int x = random.nextInt(width);
        int y = random.nextInt(height);
        int xl = random.nextInt(20 * 5);
        int yl = random.nextInt(30 * 5);
        g.drawLine(x, y, x + xl, y + yl);
    }

    private static Font getFont(Boolean forSign) {
        List<String> fonts = new ArrayList<>();
        List<Integer> fontStyles = new ArrayList<>();
        fontStyles.add(0);
        fontStyles.add(1);
        fontStyles.add(2);
        fonts.add(Font.DIALOG);
        fonts.add(Font.DIALOG_INPUT);
        fonts.add(Font.SANS_SERIF);
        fonts.add(Font.SERIF);
        fonts.add(Font.MONOSPACED);
        Collections.shuffle(fonts);
        Collections.shuffle(fontStyles);
        int fontStyle = forSign ? 1 : fontStyles.get(0);
        return new Font(fonts.get(0), fontStyle, 18 * 10);
    }


}